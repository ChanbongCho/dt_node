var express = require('express');
var router = express.Router();
const { exec } = require("child_process");

router.get('/shutdown', function(req, res, next) {
    exec("shutdown -h now", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
    res.send('Hello');
});

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;